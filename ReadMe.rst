Read Me
=======

This is general dictionary files used by PyCharm.
**Do not put words specific words to project here!**
Put specific words to project in the ``.dic`` file in the project folder instead.


Reason
------

- Spell check is necessary.
- There's too many words missed by bundled dictionaries.
- I love clean green check mark!
